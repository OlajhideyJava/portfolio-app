import { Component } from '@angular/core';
import { ProfileComponent } from './profile/profile.component';
import { ContactComponent } from './contact/contact.component';
import { SkillComponent } from './skill/skill.component';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
   date = new Date();
   year = this.date.getFullYear();
 
}
